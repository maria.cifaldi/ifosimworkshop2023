# Virtual Maps

## Useful links
### Zernikes
 * L. Tao [code snippets](https://git.ligo.org/liu.tao/code-snippet/-/tree/main/Mirror%20Map%20Sim), [paper presentation slides](https://dcc.ligo.org/DocDB/0169/G2001228), [paper](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.102.122002?casa_token=YL67D3b5XIAAAAAA%3AJ7Y5INe2V8SC2wWgDWQTWK7RqVW29AjWNFEzXksE5wd-OpEd_PYeD_xyHeREBF2u8TJO6faj3cLtgg)
